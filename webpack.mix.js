let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/css')
   .scripts([
   			'resources/assets/js/countdown.js',
   			'resources/assets/js/burger.js',
   			'resources/assets/js/timespan.js',
   			'resources/assets/js/playAudio.js',
   			], 'public/js/app.js');
