<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Chravy Club</title>

        <!-- Fonts -->
        

        <!-- Styles -->
        <link rel="stylesheet" href="/css/app.css"/>

        <!-- Scripts -->
        <script type="text/javascript" src="/js/app.js"></script>
        
    </head>
    <body>
        <section class="hero is-fullheight is-dark">
          <div class="hero-body">
            <div class="container">
              <h1 class="title has-text-centered">
                <div class="columns">
                  <div class="column is-one-third is-offset-one-third">
                    @if(session('message'))
                        {{session('message')}}
                    @endif
                    <img 
                      src="/images/failure_vectorized.png" 
                      class="failure"
                      @if(!is_null($state))
                        onclick="playAudio('{{$state->audio}}');"
                      @endif
                      alt="Click Me"
                      title="Click Me"
                    >
                  </div>
                </div>
              </h1>
              @if( !is_null($state) && !in_array($state->id, [3,4]))
                <div class="columns has-text-centered">
                  <div class="column">
                    <h1 class="title"><span id="months"></span></h1>
                    <h2 class="subtitle">Months</h2>
                  </div>
                  <div class="column">
                    <h1 class="title"><span id="weeks"></span></h1>
                    <h2 class="subtitle">Weeks</h2>
                  </div>
                  <div class="column">
                    <h1 class="title"><span id="days"></span></h1>
                    <h2 class="subtitle">Days</h2>
                  </div>
                  <div class="column">
                    <h1 class="title"><span id="hours"></span></h1>
                    <h2 class="subtitle">Hours</h2>
                  </div>
                  <div class="column">
                    <h1 class="title"><span id="minutes"></span></h1>
                    <h2 class="subtitle">Minutes</h2>
                  </div>
                  <div class="column">
                    <h1 class="title"><span id="seconds"></span></h1>
                    <h2 class="subtitle">Seconds</h2>
                  </div>
                </div>
                
              @endif
              <br />
              @if( is_null($state) || !in_array($state->id, [3,4]) )
              <h2>
                <form action="password" method="POST">
                  {{csrf_field()}}
                  <div class="field has-addons has-addons-centered">
                    <div class="control">
                      <input class="input is-large" type="password" name="password" required>
                    </div>
                    <div class="control">
                      <button  type="submit" class="button is-danger is-large is-outlined">
                        Submit
                      </button>
                    </div>
                  </div>
                </form>
              </h2>
              @endif
              <br />
              <div id="replay">
                @if( !is_null($state) && in_array($state->id, [1,2]))
                    <div class="columns has-text-centered">
                        <div class="column is-one-third">
                            @if( $state->id == 1)
                              <button class="button is-danger is-outlined" onclick="playAudio('6UBaGNdH')">Intro</button>
                            @endif
                            @if( $state->id == 2)
                              <button class="button is-danger is-outlined" onclick="playAudio('9ad5ZwNU')">Intro</button>
                            @endif
                        </div>
                        <div class="column is-one-third">
                            <button class="button is-danger is-outlined" onclick="playAudio('ZhKtDT63')">Game</button>
                        </div>
                        <div class="column is-one-third">
                            <button class="button is-danger is-outlined" onclick="playAudio('Rhy46zvQ')">Hint Tokens</button>
                        </div>
                    </div>
                    <div class="columns has-text-centered">
                      <div class="column is-one-third is-offset-one-third">
                        <button class="button is-danger is-outlined" onclick="playAudio('3QSGz2Rd')">First Clue</button>
                      </div>
                    </div>
                  @endif
              </div>
            </div>
          </div>
        </section>
    </body>
</html>
