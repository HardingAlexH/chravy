document.addEventListener('DOMContentLoaded', function () {
var timespan =
          countdown(
            new Date(2018, 02, 16, 12, 0, 0, 0),
            function(ts) {
              document.getElementById('months').innerHTML = ts.months;
              document.getElementById('weeks').innerHTML = ts.weeks;
              document.getElementById('days').innerHTML = ts.days;
              document.getElementById('hours').innerHTML = ts.hours;
              document.getElementById('minutes').innerHTML = ts.minutes;
              document.getElementById('seconds').innerHTML = ts.seconds;
            },
            countdown.MONTHS|countdown.WEEKS|countdown.DAYS|countdown.HOURS|countdown.MINUTES|countdown.SECONDS
            );
});