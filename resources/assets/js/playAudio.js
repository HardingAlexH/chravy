function playAudio(location){
	if( typeof audio == "undefined" || audio.paused ){
		audio = new Audio('/audio/'+location+".mp3");
		audio.play();
	}
}