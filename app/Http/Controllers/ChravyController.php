<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;
use App\Attempt;
use Carbon\Carbon;

class ChravyController extends Controller
{
    /**
     * returns initial view with countdown timer and password input
     *
     * @return view
     **/
    public function home()
    {
        // determining state based on passwords
        $state = null;
        $filename = null;
        $state = State::where('active', 1)->first();

        // check to see if time expired
        if (Carbon::now() > Carbon::createFromFormat('Y-m-d H', '2018-03-16 12')) {
            $state->active = 0;
            $state->save();
            $state = State::where('id', 4)->first();
            $state->active = 1;
            $state->save();
        }
        return view('home', compact('state', 'filename'));
    }

    /**
     * Records the attempted input, changes state if necessary
     *
     * @return void
     * @author
     **/
    public function password(Request $request)
    {

        // shimming in csv dump
        if ($request->get('password') == "ItCanTravelAnywhere") {
            $dump = \Excel::create('chravy_attempts_'.Carbon::now(), function ($excel) {
                $excel->sheet('Sheetname', function ($sheet) {
                    $sheet->fromArray(Attempt::select('text', 'updated_at')->get());
                });
            })->export('csv');
            return $dump;
        }


        $attempt = Attempt::create(['text' => $request->get('password')]);
        $input = strtolower($request->get('password'));
        $currentState = State::where('active', 1)->first();
        $newState = State::where('password', $input)->first();

        // no state is set yet
        if (!$currentState && $newState) {
            $newState->active = 1;
            $newState->save();
            $message = null;
        } elseif (!$currentState && !$newState) {
            $message = "Not good enough.";
        } elseif (!is_null($newState) && $currentState->password !== $input) {
            $currentState->active = 0;
            $currentState->save();
            $newState->active = 1;
            $newState->save();
            $message = null;
        } else {
            $message = "Not good enough.";
        }
        return redirect('/')->with(['message' => $message]);
    }
}
