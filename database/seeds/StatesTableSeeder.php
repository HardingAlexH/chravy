<?php

use Illuminate\Database\Seeder;
use App\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $state1 = State::create([
                'id' => 1,
                'title' => 'Masquerade Dinner Success',
                'active' => 0,
                'audio' => '3Wc3cx95',
                'password' => 'raspberry',
            ]);

        $state2 = State::create([
                'id' => 2,
                'title' => 'Masquerade Dinner Failure',
                'active' => 0,
                'audio' => 'A4R3uDU4',
                'password' => 'iamaloser',
            ]);

        $state3 = State::create([
                'id' => 3,
                'Title' => 'Game Success',
                'active' => 0,
                'audio' => 'wcgER7jQ',
                'password' => 'chravywins',
            ]);

        $state4 = State::create([
                'id' => 4,
                'Title' => 'Game Failure',
                'active' => 0,
                'audio' => 'P63gSwMj',
                'password' => 'raspberrywins',
            ]);
    }
}
